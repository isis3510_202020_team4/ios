import UIKit

class TimeLabel: UILabel {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    func commonInit() {
        self.layer.cornerRadius = 5
        self.layer.masksToBounds = true
        self.layer.borderColor = #colorLiteral(red: 0.8588235294, green: 0.8980392157, blue: 0.9725490196, alpha: 1)
        self.textColor = UIColor.white
    }
}
