//
//  AuthRepo.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 8/11/20.
//

import Foundation

class AuthRepo {
    private var firebaseAuth: FirebaseAuthentication?
    var firestore = FirebaseDB.darInstancia()
    
    init() {
        firebaseAuth = FirebaseAuthentication.darInstancia()
    }
    
    func registerUser(user: Patient, listener: ServicesListener, label: String){
        firebaseAuth?.registerUser(pUser: user, listener: listener, label: label)
    }
    
    func createUser(uid:String, user: Patient, listener: ServicesListener, label: String){
        let coleccion: [(String, String?)] = [("users", uid)]
        var datos = [String : Any]()
        datos["name"] = user.name
        datos["email"] = user.email
        datos["insurance"] = user.insurance
        datos["birthDate"] = user.birthDate
        datos["gender"] = user.gender
        datos["numFailsMedsTaken"] = 0
        datos["numMedsTaken"] = 0
        datos["outHouse"] = 0
        firestore.addDocument(rutaDocumento: coleccion, nuevoDoc: datos, listener: listener, label: label, objeto: Patient.self)
    }
    
    func loginUser(user: Patient, listener: ServicesListener, label: String){
        firebaseAuth?.loginWithEmail(pUser: user, listener: listener, label: label)
    }
}
