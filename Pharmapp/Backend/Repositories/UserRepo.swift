//
//  UserRepo.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 15/11/20.
//

import Foundation
class UserRepo {
    var firestore = FirebaseDB.darInstancia()
    
    func getUser(uid: String, listener: ServicesListener, label: String) {
        var coleccion: [(String, String?)] = [("users", uid)]
        firestore.getDocument(rutaDocumento: coleccion, listener: listener, label: label, clase: Patient.self)
    }
    
    func getUserMedicines(uid: String, listener: ServicesListener, label: String){
        var coleccion: [(String, String?)] = [("users", uid)]
        coleccion.append(("medicine", nil))
        firestore.getCollection(rutaColeccion: coleccion, listener: listener, label: label, clase: Medicine.self)
    }
}
