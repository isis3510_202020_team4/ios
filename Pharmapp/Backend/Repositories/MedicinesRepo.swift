//
//  MedicinesRepo.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 23/10/20.
//

import Foundation
class MedicinesRepo {
    
    var firestore = FirebaseDB.darInstancia()
    var functions = FirebaseCloudFunctions.darInstancia()
    
    func getMedicines(listener: ServicesListener, label: String){
        let coleccion: [(String, String?)] = [("medicines", nil)]
        firestore.getCollection(rutaColeccion: coleccion, listener: listener, label: label, clase: Medicine.self)
    }
    
    func getMedicineByBarCode(listener: ServicesListener, label: String, barCode: String){
        let coleccion: [(String, String?)] = [("medicines", nil)]
        firestore.getCollectionSpecificField(rutaColeccion: coleccion, listener: listener, label: label, clase: Medicine.self, campo: "id", valor: barCode)
    }
    
    func addMedicine(listener: ServicesListener, label: String, medicine: Medicine){
        var datos = [String : Any]()
        datos["uid"] = medicine.uidDocumento
        functions.llamar(funcion: "addMedicine", con: datos, listener: listener, label: label)
    }
}
