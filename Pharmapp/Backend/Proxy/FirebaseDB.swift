//
//  FirebaseDB.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 22/10/20.
//

import Foundation
import Firebase

class FirebaseDB: BaseProxy{
    
    //---------------------------------------------
    // MARK: - Atributos
    //---------------------------------------------
    
    private var firestore = Firestore.firestore()
    
    private var registroListeners = [ListenerRegistration]()
    
    static private var instancia: FirebaseDB!
    
    //---------------------------------------------
    // MARK: - Constructor
    //---------------------------------------------
    
    static func darInstancia() -> FirebaseDB{
        if instancia == nil{
            instancia = FirebaseDB()
        }
        return instancia
    }
    
    func liberarListeners() {
        for registro in registroListeners{
            registro.remove()
        }
    }
    
    //---------------------------------------------
    // MARK: - Métodos
    //---------------------------------------------
    
    func addDocumentAutoID(rutaDocumento: [(String, String?)], nuevoDoc: [String: Any], listener: ServicesListener, label: String, clase: Any){
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
        }
        
        let ruta = darRuta(con: rutaDocumento)
        
        firestore.collection(ruta).addDocument(data: nuevoDoc){error in
            if error != nil{
                print("error: \(label): \(error?.localizedDescription)")
                listener.fail(label: label)
            }
            else{
                listener.success(label: label, objeto: clase)
            }
        }
    }
    
    func addDocument(rutaDocumento: [(String, String?)], nuevoDoc: [String: Any], listener: ServicesListener, label: String, objeto: Any){
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
        }
        let ruta = darRuta(con: rutaDocumento)
        firestore.document(ruta).setData(nuevoDoc){error in
            if error != nil{
                listener.fail(label: label)
            }
            else{
                listener.success(label: label, objeto: objeto)
            }
        }
    }
    
    func getDocument<T:BaseModel>(rutaDocumento: [(String, String?)], listener: ServicesListener, label: String, clase: T.Type){
        //Revisa la conexión a internet
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        //Construye la ruta
        let ruta = darRuta(con: rutaDocumento)
        
        //Se suscribe a actualizaciones del documento
        let snapshot = firestore.document(ruta).addSnapshotListener { [unowned listener] (snapshot, error) in
            if error != nil{
                listener.fail(label: Constants.Errors.LISTENER_ERROR)
            }
            if let document = snapshot{
                if document.exists{
                    let result: T = T(mapa: document.data()!)
                    result.uidDocumento = document.documentID
                    listener.update(label: label, objeto: result)
                }
            }
        }
        // Guardar esto para acabar la suscripcion más adelante
        registroListeners.append(snapshot)
        
        //Obtiene el documento
        firestore.document(ruta).getDocument {(snapshot, error) in
            if error != nil{
                listener.fail(label: Constants.Errors.LISTENER_ERROR)
            }
            if let document = snapshot{
                if document.exists{
                    let result: T = T(mapa: document.data()!)
                    result.uidDocumento = document.documentID
                    listener.success(label: label, objeto: result)
                }
                else{
                    listener.fail(label: label)
                }
            }
        }
    }
    
   
    func getCollection<T:BaseModel>(rutaColeccion: [(String, String?)], listener: ServicesListener, label: String, clase: T.Type, limitarA: Int? = nil, ordenarPor: String? = nil, ordenAscendente: Bool = false){
        //Revisa la conexión a internet
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        //construye la ruta
         let ruta = darRuta(con: rutaColeccion)
        
        //Genera la consulta
        var consulta: Query = firestore.collection(ruta)
        if let ordenar = ordenarPor{
            consulta = consulta.order(by: ordenar, descending: !ordenAscendente)
        }
        if let limite = limitarA{
            consulta = consulta.limit(to: limite)
        }
        
        //Se suscribe a actualizaciones de la coleccion
        let snapshot = consulta.addSnapshotListener {(snapshot, error) in
            if error != nil{
                listener.fail(label: Constants.Errors.LISTENER_ERROR)
            }
            if let snap = snapshot{
                var lista = [T]()
                for documento in snap.documents{
                    let objeto = T(mapa: documento.data())
                    objeto.uidDocumento = documento.documentID
                    lista.append(objeto)
                }
                listener.update(label: label, objeto: lista)
            }
        }
        // Guardar esto para acabar la suscripcion más adelante
        registroListeners.append(snapshot)
        
        //Obtiene la coleccion
        consulta.getDocuments() { (snapshot, error) in
            if error != nil {
                listener.fail(label: Constants.Errors.LISTENER_ERROR)
            }
            else if let snap = snapshot{
                var lista = [T]()
                for documento in snap.documents{
                    let objeto = T(mapa: documento.data())
                    objeto.uidDocumento = documento.documentID
                    lista.append(objeto)
                }
                listener.success(label: label, objeto: lista)
            }
        }
    }
    
    func getCollectionSpecificField<T:BaseModel>(rutaColeccion: [(String, String?)], listener: ServicesListener, label: String, clase: T.Type, campo: String, valor: Any, limitarA: Int? = nil, ordenarPor: String? = nil, ordenAscendente: Bool = false){
        //Revisa la conexión a internet
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        //construye la ruta
        let ruta = darRuta(con: rutaColeccion)
        
        //Genera la consulta
        var consulta: Query = firestore.collection(ruta).whereField(campo, isEqualTo: valor)
        //consulta.order(by: campo, descending: false)
        if let ordenar = ordenarPor{
            consulta = consulta.order(by: ordenar, descending: !ordenAscendente)
        }
        if let limite = limitarA{
            consulta = consulta.limit(to: limite)
        }
        
        //Se suscribe a actualizaciones de la coleccion
        let snapshot = consulta.addSnapshotListener {[unowned listener] (snapshot, error) in
            if error != nil{
                listener.fail(label: Constants.Errors.LISTENER_ERROR)
            }
            if let snap = snapshot{
                var lista = [T]()
                for documento in snap.documents{
                    let objeto = T(mapa: documento.data())
                    objeto.uidDocumento = documento.documentID
                    lista.append(objeto)
                }
                listener.update(label: label, objeto: lista)
            }
        }
        // Guardar esto para acabar la suscripcion más adelante
        registroListeners.append(snapshot)
        
        //Obtiene la coleccion
        consulta.getDocuments { (snapshot, error) in
            if error != nil{
                listener.fail(label: Constants.Errors.LISTENER_ERROR)
            }
            if let snap = snapshot{
                var lista = [T]()
                for documento in snap.documents{
                    let objeto = T(mapa: documento.data())
                    objeto.uidDocumento = documento.documentID
                    lista.append(objeto)
                }
                listener.success(label: label, objeto: lista)
            }
        }
    }
    
    func updateValue(rutaDocumento: [(String, String?)], valorModificar: [String: Any], listener: ServicesListener?, label: String){
        //Revisa la conexión a internet
        if !conexionInternet() && listener != nil{
            listener?.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        
        //Construye la ruta
        let ruta = darRuta(con: rutaDocumento)
        //Actualiza el documento
        firestore.document(ruta).updateData(valorModificar){ error in
            if error != nil{
                listener?.fail(label: label)
            }
            else{
                listener?.success(label: label, objeto: nil)
            }
        }
    }
    
    func deleteDocument(rutaDocumento: [(String, String?)], listener: ServicesListener, label: String){
        //Revisa la conexión a internet
        if !conexionInternet(){
            listener.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        
        //Construye la ruta
        let ruta = darRuta(con: rutaDocumento)
        
        //Borra el documento
        firestore.document(ruta).delete { (error) in
            if error != nil{
                listener.fail(label: label)
            }
            else{
                listener.success(label: label, objeto: nil)
            }
        }
    }
    
    private func darRuta(con rutaDocumento: [(String, String?)]) -> String{
        var ruta = ""
        for (key, value) in rutaDocumento{
            ruta += "\(key)/"
            if let value = value{
                ruta += "\(value)/"
            }
        }
        if(ruta.count > 0){
            ruta.removeLast()
        }
        print("ACCEDIENDO A RUTA: \(ruta)")
        return ruta
    }
}

