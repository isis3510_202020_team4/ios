//
//  FirebaseAutenticacion.swift
//
import Foundation
import FirebaseAuth
import FBSDKCoreKit
import GoogleSignIn
import AuthenticationServices

class FirebaseAuthentication: BaseProxy{
    //---------------------------------------------
    // MARK: - Atributos
    //---------------------------------------------
    
    static private var instancia: FirebaseAuthentication!
    
    //---------------------------------------------
    // MARK: - Constructor
    //---------------------------------------------
    override private init(){}
    
    static func darInstancia() -> FirebaseAuthentication{
        if instancia == nil{
            instancia = FirebaseAuthentication()
        }
        return instancia
    }
    
    //---------------------------------------------
    // MARK: - Métodos
    //---------------------------------------------
    func registerUser<T:BaseModel>(pUser: T, listener: ServicesListener, label: String){
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        let user = pUser as! Patient
        Auth.auth().createUser(withEmail: user.email!, password: user.password!) { (result, error) in
            guard let user = result?.user else {
                listener.fail(label: Constants.Errors.WRONG_INFO_REGISTER)
                return
            }
            self.updateUser(user: user, listener: listener, label: label)
        }
    }
    
    func loginWithEmail<T:BaseModel>(pUser: T, listener: ServicesListener, label: String){
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        let user = pUser as! Patient
        Auth.auth().signIn(withEmail: user.email!, password: user.password!) { (user, error) in
            if error != nil{
                listener.fail(label: Constants.Errors.WRONG_INFO_LOGIN)
            }
            else{
                self.updateUser(user: user!.user, listener: listener, label: label)
            }
        }
    }
    
    
    func loginWithGoogle(user: GIDGoogleUser, listener: ServicesListener, label: String) {
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        guard let authentication = user.authentication else {
            print("falla en autenticacion")
            listener.fail(label: Constants.Errors.GOOGLE_AUTH_ERROR)
            return
        }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        Auth.auth().signIn(with: credential) { (result, error) in
            if error != nil {
                print("error Auth.signIn: \(error?.localizedDescription)")
                listener.fail(label: Constants.Errors.GOOGLE_AUTH_ERROR)
            }
            else{
                self.updateUser(user: result!.user, listener: listener, label: label)
            }
        }
    }
    
    private func updateUser(user: User, listener: ServicesListener, label: String) {
        let usuario = Patient()
        usuario.uidDocumento = user.uid
        usuario.email = user.email
        listener.success(label: label, objeto: usuario)
    }
}
