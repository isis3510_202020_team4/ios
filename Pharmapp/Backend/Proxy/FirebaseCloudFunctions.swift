//
//  FirebaseCloudFunctions.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 13/11/20.
//

import Foundation
import FirebaseFunctions

class FirebaseCloudFunctions: BaseProxy{
    //---------------------------------------------
    // MARK: - Atributos
    //---------------------------------------------
    private var functions = Functions.functions()
    
    static private var instancia: FirebaseCloudFunctions!
    
    //---------------------------------------------
    // MARK: - Constructor
    //---------------------------------------------
    
    static func darInstancia() -> FirebaseCloudFunctions{
        if instancia == nil{
            instancia = FirebaseCloudFunctions()
        }
        return instancia
    }
    
    func llamar(funcion: String, con datos: [String: Any], listener: ServicesListener, label: String){
        if(!conexionInternet()){
            listener.fail(label: Constants.Errors.NO_INTERNET)
            return
        }
        //functions.useFunctionsEmulator(origin: "http://localhost:5001")
        functions.httpsCallable(funcion)
            .call(datos) { (result, error) in
                if error != nil {
                    print("error: \(error?.localizedDescription)")
                    listener.fail(label: Constants.Errors.CLOUD_ERROR)
                    return
                }
                else{
                    listener.success(label: label, objeto: result?.data)
                }
        }
    }
}
