//
//  Medicine.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 23/10/20.
//

import Foundation
class Medicine: BaseModel, NSCoding{
    
    //---------------------------------------------
    // MARK: - Attributes
    //---------------------------------------------
    
    var name: String?
    var descriptionMed: String?
    var barCode: Int?
    var activePrinciple: String?
    var tag: String?
    var brand: String?
    var time: String?
    
    //---------------------------------------------
    // MARK: - Constructors
    //---------------------------------------------
    
    required convenience init?(coder: NSCoder) {
        self.init()
        uidDocumento = coder.decodeObject(forKey: "uidDocumento") as? String
        name = coder.decodeObject(forKey: "name") as? String
        descriptionMed = coder.decodeObject(forKey: "descriptionMed") as? String
        barCode = coder.decodeObject(forKey: "barCode") as? Int
        activePrinciple = coder.decodeObject(forKey: "activePrinciple") as? String
        tag = coder.decodeObject(forKey: "tag") as? String
        brand = coder.decodeObject(forKey: "brand") as? String
        time = coder.decodeObject(forKey: "time") as? String
    }

    /// Sobrescripcion. Ver en protocolo `BaseModel`
    required convenience init(mapa: [String: Any]){
        self.init()
        name = mapa["name"] as? String
        descriptionMed = mapa["descriptionMed"] as? String
        barCode = mapa["barCode"] as? Int
        activePrinciple = mapa["activePrinciple"] as? String
        tag = mapa["tag"] as? String
        brand = mapa["brand"] as? String
        time = mapa["time"] as? String
    }
    
    //---------------------------------------------
    // MARK: - Functions
    //---------------------------------------------
    
    func encode(with coder: NSCoder) {
        coder.encode(uidDocumento, forKey: "uidDocumento")
        coder.encode(name, forKey: "name")
        coder.encode(descriptionMed, forKey: "descriptionMed")
        coder.encode(barCode, forKey: "barCode")
        coder.encode(activePrinciple,forKey: "activePrinciple")
        coder.encode(tag,forKey: "tag")
        coder.encode(brand,forKey: "brand")
        coder.encode(time,forKey: "time")
    }
}
