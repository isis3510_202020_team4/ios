//
//  User.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 24/10/20.
//

import Foundation
class Patient: BaseModel, NSCoding{
    
    //---------------------------------------------
    // MARK: - Attributes
    //---------------------------------------------
    
    var name: String?
    var email: String?
    var password: String?
    var city: String?
    var birthDate: String?
    var gender: String?
    var insurance: String?
    
    //---------------------------------------------
    // MARK: - Constructors
    //---------------------------------------------
    
    required convenience init?(coder: NSCoder) {
        self.init()
        uidDocumento = coder.decodeObject(forKey: "uidDocumento") as? String
        name = coder.decodeObject(forKey: "name") as? String
        email = coder.decodeObject(forKey: "email") as? String
        password = coder.decodeObject(forKey: "password") as? String
        city = coder.decodeObject(forKey: "city") as? String
        birthDate = coder.decodeObject(forKey: "birthDate") as? String
        gender = coder.decodeObject(forKey: "gender") as? String
        insurance = coder.decodeObject(forKey: "insurance") as? String
    }

    /// Sobrescripcion. Ver en protocolo `BaseModel`
    required convenience init(mapa: [String: Any]){
        self.init()
        name = mapa["name"] as? String
        birthDate = mapa["birthDate"] as? String
        insurance = mapa["insurance"] as? String
        email = mapa["email"] as? String
        password = mapa["password"] as? String
        city = mapa["city"] as? String
        gender = mapa["gender"] as? String
    }
    
    //---------------------------------------------
    // MARK: - Functions
    //---------------------------------------------
    
    func encode(with coder: NSCoder) {
        coder.encode(uidDocumento, forKey: "uidDocumento")
        coder.encode(name, forKey: "name")
        coder.encode(gender, forKey: "gender")
        coder.encode(birthDate, forKey: "birthDate")
        coder.encode(email, forKey: "email")
        coder.encode(password, forKey: "password")
        coder.encode(insurance, forKey: "insurance")
        coder.encode(city, forKey: "city")
    }
}
