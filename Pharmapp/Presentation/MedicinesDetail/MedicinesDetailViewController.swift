//
//  MedicinesDetailViewController.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 17/10/20.
//

import UIKit

class MedicinesDetailViewController: BaseViewController, ServicesListener {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        drawItems()
        // Do any additional setup after loading the view.
    }
    
    /*
     Outlets
     */
    @IBOutlet weak var shadow: UIView!
    @IBOutlet weak var medsContainer: UIView!
    @IBOutlet weak var timeContainer: UIView!
    @IBOutlet weak var whiteCircle: UIView!
    @IBOutlet weak var btnAddMed: UIButton!
    @IBOutlet weak var blueCircle: UIView!
    @IBOutlet weak var btn1Time: UIButton!
    @IBOutlet weak var btn2Time: UIButton!
    @IBOutlet weak var btn3Time: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblTag: UILabel!
    @IBOutlet weak var lblMiniDescription: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    var actualMedicine = Medicine()
    let repoMedicines = MedicinesRepo()
    /*
     Draw
     */
    func drawItems(){
        shadow.layer.cornerRadius = 25
        medsContainer.layer.cornerRadius = 25
        timeContainer.layer.cornerRadius = 25
        whiteCircle.layer.cornerRadius = whiteCircle.frame.width/2
        blueCircle.layer.cornerRadius = blueCircle.frame.width/2
        btnAddMed.layer.cornerRadius = 25
        lblTag.layer.cornerRadius = 12
        lblTag.layer.masksToBounds = true
        btn1Time.layer.cornerRadius = 25
        btn2Time.layer.cornerRadius = 25
        btn3Time.layer.cornerRadius = 25
        lblName.text = actualMedicine.name
        lblTag.text = actualMedicine.tag
        lblDescription.text = actualMedicine.descriptionMed
        lblMiniDescription.text = actualMedicine.brand
        lblTime.text = actualMedicine.time
        let cache = NSCache<NSString, Medicine>()
        if let cachedVersion = cache.object(forKey: "Medicine") {
            actualMedicine = cachedVersion
        } else {
            let myObject = Medicine()
            cache.setObject(myObject, forKey: "Medicine")
        }
    }
    
    
    
    @IBAction func addMedicine(_ sender: Any) {
        if actualMedicine.name != nil{
            aparecerProgressBar(color: Constants.Colors.buttonsGreen)
            repoMedicines.addMedicine(listener: self, label: "addMedicine", medicine: actualMedicine)
        }
    }
    
    
    
    func success(label: String, objeto: Any?) {
        desaparecerProgressBar()
        if label == "addMedicine" {
            let result = objeto as! String
            switch result {
            case Constants.CloudFunctions.ADDED:
                let alert = UIAlertController(title: "Medicine Added", message: Constants.CloudFunctions.ADDED_DESCRIPTION, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case Constants.CloudFunctions.NO_ID_MEDICINE:
                let alert = UIAlertController(title: "No Medicine Found", message: Constants.CloudFunctions.NO_ID_MEDICINE_DESCRIPTION, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case Constants.CloudFunctions.NO_MEDICINE:
                let alert = UIAlertController(title: "No Medicine Found", message: Constants.CloudFunctions.NO_MEDICINE_DESCRIPTION, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case Constants.CloudFunctions.RISK:
                let alert = UIAlertController(title: "Risky Medicine", message: Constants.CloudFunctions.RISK_DESCRIPTION, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            case Constants.CloudFunctions.UNAUTHORIZED:
                let alert = UIAlertController(title: "Unauthorized", message: Constants.CloudFunctions.UNAUTHORIZED_DESCRIPTION, preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            default:
                let alert = UIAlertController(title: "Something went wrong", message: "We are having issues with our platform, please try again", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func fail(label: String) {
        desaparecerProgressBar()
        switch label {
        case Constants.Errors.CLOUD_ERROR:
            let alert = UIAlertController(title: "Something went wrong", message: "We are having issues with our platform, please try again", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case Constants.Errors.NO_INTERNET:
            let alert = UIAlertController(title: "No Internet", message: "Seems like you don´t have a stable internet connection, we will save your data meanwhile you recover your internet connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            let alert = UIAlertController(title: "Something went wrong", message: "We are having issues with our platform, please try again", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func update(label: String, objeto: Any?) {
        //
    }
}
