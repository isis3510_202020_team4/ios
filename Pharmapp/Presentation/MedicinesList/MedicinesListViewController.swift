//
//  MedicinesListViewController.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 16/10/20.
//

import UIKit
class MedicinesListViewController: BaseViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, ServicesListener {

    //---------------------------------------------
    // MARK: - Attributes
    //---------------------------------------------
    
    @IBOutlet weak var btnAll: UIButton!
    @IBOutlet weak var btnVitamins: UIButton!
    @IBOutlet weak var btnPills: UIButton!
    @IBOutlet weak var tfSearch: CustomTextField!
    @IBOutlet weak var collectionMedicines: UICollectionView!
    let repoMedicines = MedicinesRepo()
    var arrayMeds:Array<Medicine> = []
    var arrayCopy:Array<Medicine> = []

    //---------------------------------------------
    // MARK: - Init
    //---------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        drawItems()
        getMedicines()
    }
    
    func drawItems(){

        btnAll.layer.cornerRadius = 25
        btnVitamins.layer.cornerRadius = 25
        btnPills.layer.cornerRadius = 25
        
        tfSearch.layer.masksToBounds = true
        tfSearch.layer.cornerRadius = 25
        tfSearch.layer.borderWidth = 2
        tfSearch.layer.borderColor = UIColor.white.cgColor
        tfSearch.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: tfSearch.frame.height))
        tfSearch.leftViewMode = .always
        tfSearch.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: tfSearch.frame.height))
        tfSearch.rightViewMode = .always
        
        collectionMedicines.delegate = self
        collectionMedicines.dataSource = self
        collectionMedicines.backgroundColor = .none
        
        let toolbarSearch = UIToolbar()
        toolbarSearch.sizeToFit()
        let doneBtnTextfield = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(search))
        toolbarSearch.setItems([doneBtnTextfield], animated: true)
        tfSearch.inputAccessoryView = toolbarSearch
    }
    
    @objc func search(){
        if tfSearch.text != nil && !tfSearch.text!.isEmpty {
            arrayMeds = arrayCopy
            let filtro = arrayMeds.filter { (medicine) -> Bool in
                return (medicine.name?.contains(tfSearch.text!))!
            }
            arrayMeds = filtro
            collectionMedicines.reloadData()
            self.view.endEditing(true)
        } else {
            arrayMeds = arrayCopy
            collectionMedicines.reloadData()
            self.view.endEditing(true)
        }
    }
    
    func getMedicines(){
        repoMedicines.getMedicines(listener: self, label: Constants.Labels.GET_MEDICINES)
    }
    
    //---------------------------------------------
    // MARK: - Collection View Methods
    //---------------------------------------------
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrayMeds.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionMedicines.dequeueReusableCell(withReuseIdentifier: "medicinesCell", for: indexPath) as! MedicineCollectionViewCell
        let actualMedicine = arrayMeds[indexPath.row]
        cell.name.text = actualMedicine.name
        cell.type.text = actualMedicine.tag
        cell.type.layer.cornerRadius = 12
        cell.type.layer.masksToBounds = true
        cell.circleSelect.layer.cornerRadius = cell.circleSelect.frame.width/2
        cell.layer.cornerRadius = 50
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let screenSize = UIScreen.main.bounds
        return CGSize(width: screenSize.width * 0.88, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.performSegue(withIdentifier: "detailMed", sender: indexPath.row)
    }
    
    //---------------------------------------------
    // MARK: - Segues
    //---------------------------------------------
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailMed" {
            let medClicked = sender as! Int
            let actualMed = arrayMeds[medClicked]
            let detailScreen: MedicinesDetailViewController = segue.destination as! MedicinesDetailViewController
            detailScreen.actualMedicine = actualMed
        }
    }
    
    //---------------------------------------------
    // MARK: - Buttons
    //---------------------------------------------
    
    @IBAction func allFilter(_ sender: Any) {
        btnAll.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.9137254902, blue: 0.7803921569, alpha: 1)
        btnAll.setTitleColor(UIColor.white, for: .normal)
        btnPills.backgroundColor = UIColor.white
        btnPills.setTitleColor(UIColor.black, for: .normal)
        btnVitamins.backgroundColor = UIColor.white
        btnVitamins.setTitleColor(UIColor.black, for: .normal)
        arrayMeds = arrayCopy
        collectionMedicines.reloadData()
    }
    
    @IBAction func vitaminsFilter(_ sender: Any) {
        btnAll.backgroundColor = UIColor.white
        btnAll.setTitleColor(UIColor.black, for: .normal)
        btnPills.backgroundColor = UIColor.white
        btnPills.setTitleColor(UIColor.black, for: .normal)
        btnVitamins.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.9137254902, blue: 0.7803921569, alpha: 1)
        btnVitamins.setTitleColor(UIColor.white, for: .normal)
        arrayMeds = arrayCopy
        let filtro = arrayMeds.filter { (medicine) -> Bool in
            return medicine.tag == "Vitamins"
        }
        arrayMeds = filtro
        collectionMedicines.reloadData()
    }
    
    @IBAction func pillsFilter(_ sender: Any) {
        btnAll.backgroundColor = UIColor.white
        btnAll.setTitleColor(UIColor.black, for: .normal)
        btnPills.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.9137254902, blue: 0.7803921569, alpha: 1)
        btnPills.setTitleColor(UIColor.white, for: .normal)
        btnVitamins.backgroundColor = UIColor.white
        btnVitamins.setTitleColor(UIColor.black, for: .normal)
        arrayMeds = arrayCopy
        let filtro = arrayMeds.filter { (medicine) -> Bool in
            return medicine.tag == "Pills"
        }
        arrayMeds = filtro
        collectionMedicines.reloadData()
    }
    
    //---------------------------------------------
    // MARK: - Connection to repositories
    //---------------------------------------------
    
    func success(label: String, objeto: Any?) {
        if label == Constants.Labels.GET_MEDICINES {
            arrayMeds = objeto as! Array<Medicine>
            arrayCopy = arrayMeds
            collectionMedicines.reloadData()
        }
    }
    
    func fail(label: String) {
        
    }
    
    func update(label: String, objeto: Any?) {
        if label == Constants.Labels.GET_MEDICINES {
            arrayMeds = objeto as! Array<Medicine>
            collectionMedicines.reloadData()
        }
    }
}

class CustomTextField: UITextField {

    let padding = UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
