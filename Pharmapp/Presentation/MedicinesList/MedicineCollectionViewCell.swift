//
//  MedicineCollectionViewCell.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 17/10/20.
//

import UIKit

class MedicineCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var circleSelect: UIView!
}
