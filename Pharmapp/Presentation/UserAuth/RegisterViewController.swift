//
//  RegisterViewController.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 6/11/20.
//

import UIKit
import FirebaseAuth
class RegisterViewController: BaseViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, ServicesListener {

    //---------------------------------------------
    // MARK: - Outlets
    //---------------------------------------------
    
    @IBOutlet weak var btnSignup: UIButton!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var etName: UITextField!
    @IBOutlet weak var etInsurance: UITextField!
    @IBOutlet weak var etCities: UITextField!
    @IBOutlet weak var etGender: UITextField!
    @IBOutlet weak var etBirthDate: UITextField!
    @IBOutlet weak var etEmail: UITextField!
    @IBOutlet weak var etPassword: UITextField!
    let repoAuth = AuthRepo()
    var datePicker = UIDatePicker()
    var pickerGender = UIPickerView()
    var pickerCities = UIPickerView()
    var cities = ["Bogotá", "Bucaramanga", "Cali", "Medellín", "Santa Marta", "Cartagena"]
    var genders = ["Male", "Female"]
    var user = Patient()
    let cache = UserDefaults.init(suiteName: "register")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        checkSession()
        recoverUserDefaults()
        setupView()
        pickers()
    }
    
    //---------------------------------------------
    // MARK: - Setup
    //---------------------------------------------
    
    func setupView(){
        self.hideKeyboardWhenTappedAround()
        btnLogin.layer.cornerRadius = 30
        btnLogin.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        btnSignup.layer.cornerRadius = 30
        btnSignup.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        btnLogin.layer.masksToBounds = true
        btnLogin.layer.borderColor = Constants.Colors.buttonsGreen.cgColor
        btnLogin.layer.borderWidth = 1
        etName.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor: Constants.Colors.greyText])
        etInsurance.attributedPlaceholder = NSAttributedString(string: "Insurance", attributes: [NSAttributedString.Key.foregroundColor: Constants.Colors.greyText])
        etCities.attributedPlaceholder = NSAttributedString(string: "Cities", attributes: [NSAttributedString.Key.foregroundColor: Constants.Colors.greyText])
        etGender.attributedPlaceholder = NSAttributedString(string: "Gender", attributes: [NSAttributedString.Key.foregroundColor: Constants.Colors.greyText])
        etBirthDate.attributedPlaceholder = NSAttributedString(string: "Birth date", attributes: [NSAttributedString.Key.foregroundColor: Constants.Colors.greyText])
        etEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: Constants.Colors.greyText])
        etPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: Constants.Colors.greyText])
        
        let bottomLineName = CALayer()
        bottomLineName.backgroundColor = Constants.Colors.greyText.cgColor
        bottomLineName.frame = CGRect(x: 0.0, y: etName.frame.height - 1, width: etName.frame.width, height: 1.0)
        etName.borderStyle = UITextField.BorderStyle.none
        etName.layer.addSublayer(bottomLineName)
        
        let bottomLineEmail = CALayer()
        bottomLineEmail.backgroundColor = Constants.Colors.greyText.cgColor
        bottomLineEmail.frame = CGRect(x: 0.0, y: etEmail.frame.height - 1, width: etEmail.frame.width, height: 1.0)
        etEmail.borderStyle = UITextField.BorderStyle.none
        etEmail.layer.addSublayer(bottomLineEmail)
        
        let bottomLineGender = CALayer()
        bottomLineGender.backgroundColor = Constants.Colors.greyText.cgColor
        bottomLineGender.frame = CGRect(x: 0.0, y: etGender.frame.height - 1, width: etGender.frame.width, height: 1.0)
        etGender.borderStyle = UITextField.BorderStyle.none
        etGender.layer.addSublayer(bottomLineGender)
        
        let bottomLineCities = CALayer()
        bottomLineCities.backgroundColor = Constants.Colors.greyText.cgColor
        bottomLineCities.frame = CGRect(x: 0.0, y: etCities.frame.height - 1, width: etCities.frame.width, height: 1.0)
        etCities.borderStyle = UITextField.BorderStyle.none
        etCities.layer.addSublayer(bottomLineCities)
        
        let bottomLinePassword = CALayer()
        bottomLinePassword.backgroundColor = Constants.Colors.greyText.cgColor
        bottomLinePassword.frame = CGRect(x: 0.0, y: etPassword.frame.height - 1, width: etPassword.frame.width, height: 1.0)
        etPassword.borderStyle = UITextField.BorderStyle.none
        etPassword.layer.addSublayer(bottomLinePassword)
       
        let bottomLineInsurance = CALayer()
        bottomLineInsurance.backgroundColor = Constants.Colors.greyText.cgColor
        bottomLineInsurance.frame = CGRect(x: 0.0, y: etInsurance.frame.height - 1, width: etInsurance.frame.width, height: 1.0)
        etInsurance.borderStyle = UITextField.BorderStyle.none
        etInsurance.layer.addSublayer(bottomLineInsurance)
        
        let bottomLineBirthDate = CALayer()
        bottomLineBirthDate.backgroundColor = Constants.Colors.greyText.cgColor
        bottomLineBirthDate.frame = CGRect(x: 0.0, y: etBirthDate.frame.height - 1, width: etBirthDate.frame.width, height: 1.0)
        etBirthDate.borderStyle = UITextField.BorderStyle.none
        etBirthDate.layer.addSublayer(bottomLineBirthDate)
        
        //Configuring the date picker
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(donePressedDatePicker))
        toolbar.setItems([doneBtn], animated: true)
        
        let toolbarTextField = UIToolbar()
        toolbarTextField.sizeToFit()
        let doneBtnTextfield = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(doneEditing))
        toolbarTextField.setItems([doneBtnTextfield], animated: true)
        
        etName.inputAccessoryView = toolbarTextField
        etInsurance.inputAccessoryView = toolbarTextField
        etCities.inputAccessoryView = toolbarTextField
        etGender.inputAccessoryView = toolbarTextField
        etEmail.inputAccessoryView = toolbarTextField
        etPassword.inputAccessoryView = toolbarTextField
        
        etBirthDate.inputAccessoryView = toolbar
        etBirthDate.inputView = datePicker
        datePicker.datePickerMode = .date
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = .wheels
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func donePressedDatePicker(){
        let formatter = DateFormatter()
        formatter.dateStyle = .medium
        formatter.timeStyle = .none
        etBirthDate.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    @objc func doneEditing(){
        self.view.endEditing(true)
    }
    
    func checkSession(){
        if Auth.auth().currentUser != nil {
            performSegue(withIdentifier: "registerSuccess", sender: self)
        }
    }
    
    //---------------------------------------------
    // MARK: - Pickers
    //---------------------------------------------
    
    func pickers() {
        pickerCities.delegate = self
        pickerCities.dataSource = self
        etCities.inputView = pickerCities
        etCities.delegate = self
        
        pickerGender.delegate = self
        pickerGender.dataSource = self
        etGender.inputView = pickerGender
        etGender.delegate = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView === pickerCities {
            return cities.count
        } else {
            return genders.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == pickerGender {
            return genders[row]
        } else {
            return cities[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == pickerGender {
            etGender.text = genders[row]
        } else {
            etCities.text = cities[row]
        }
    }
    
    //---------------------------------------------
    // MARK: - Buttons
    //---------------------------------------------
    
    @IBAction func signUp(_ sender: UIButton) {
        if !etName.text!.isEmpty && !etInsurance.text!.isEmpty
        && !etCities.text!.isEmpty && !etGender.text!.isEmpty
            && !etBirthDate.text!.isEmpty && !etEmail.text!.isEmpty
            && !etPassword.text!.isEmpty{
            user.name = etName.text
            user.birthDate = etBirthDate.text
            user.city = etCities.text
            user.email = etEmail.text
            user.gender = etGender.text
            user.password = etPassword.text
            user.insurance = etInsurance.text
            aparecerProgressBar(color: Constants.Colors.titlesGreen)
            repoAuth.registerUser(user: user, listener: self, label: "register")
        } else {
            let alert = UIAlertController(title: "Incomplete", message: "You must fill all the fields to continue with your register", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //---------------------------------------------
    // MARK: - Segues
    //---------------------------------------------
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     //If needed
    }
    
    //---------------------------------------------
    // MARK: - User Defaults
    //---------------------------------------------
    
    func saveUserDefaults() {
        cache!.set(etName.text, forKey: "name")
        cache!.set(etEmail.text, forKey: "email")
        cache!.set(etBirthDate.text, forKey: "birthDate")
        cache!.set(etInsurance.text, forKey: "insurance")
        cache!.set(etGender.text, forKey: "gender")
        cache!.set(etCities.text, forKey: "city")
    }
    
    func recoverUserDefaults() {
        let name = cache!.string(forKey: "name")
        if(name != nil){
            etName.text = name
        }
        let email = cache!.string(forKey: "email")
        if(email != nil){
            etEmail.text = email
        }
        let gender = cache!.string(forKey: "gender")
        if(gender != nil){
            etGender.text = gender
        }
        let city = cache!.string(forKey: "city")
        if(city != nil){
            etCities.text = city
        }
        let insurance = cache!.string(forKey: "insurance")
        if(insurance != nil){
            etInsurance.text = insurance
        }
        let birthDate = cache!.string(forKey: "birthDate")
        if(birthDate != nil){
            etBirthDate.text = birthDate
        }
    }
    
    //---------------------------------------------
    // MARK: - Listeners
    //---------------------------------------------
    
    func success(label: String, objeto: Any?) {
        if label == "register" {
            desaparecerProgressBar()
            aparecerProgressBar(color: Constants.Colors.titlesGreen)
            let userAuth = (objeto as! Patient)
            repoAuth.createUser(uid: userAuth.uidDocumento!, user: user, listener: self, label: "createUser")
        }
        if label == "createUser" {
            desaparecerProgressBar()
            cache!.removePersistentDomain(forName: "register")
            self.performSegue(withIdentifier: "registerSuccess", sender: nil)
        }
    }
    
    func fail(label: String) {
        desaparecerProgressBar()
        switch label {
        case Constants.Errors.WRONG_INFO_REGISTER:
            let alert = UIAlertController(title: "Incorrect", message: "Seems that there is a problem with your register, please try again", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case Constants.Errors.NO_INTERNET:
            let alert = UIAlertController(title: "No Internet", message: "Seems like you don´t have a stable internet connection, we will save your data meanwhile you recover your internet connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            saveUserDefaults()
        default:
            let alert = UIAlertController(title: "Error", message: "We are having issues with your request", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func update(label: String, objeto: Any?) {
        //
    }
    
}
