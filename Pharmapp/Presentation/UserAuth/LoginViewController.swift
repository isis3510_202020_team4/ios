//
//  LoginViewController.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 6/11/20.
//

import UIKit

class LoginViewController: BaseViewController, ServicesListener {
    
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    @IBOutlet weak var btnLogin: UIButton!
    let repoAuth = AuthRepo()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //self.view.addGradient()
        setupView()
    }
    
    func setupView(){
        self.hideKeyboardWhenTappedAround()
        btnLogin.layer.cornerRadius = 30
        btnLogin.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        gradientLayer.colors = [Constants.Colors.gradientStart.cgColor, Constants.Colors.gradientEnd.cgColor]
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        tfEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        tfPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
        let bottomLinePassword = CALayer()
        bottomLinePassword.backgroundColor = UIColor.white.cgColor
        bottomLinePassword.frame = CGRect(x: 0.0, y: tfPassword.frame.height - 1, width: tfPassword.frame.width, height: 1.0)
        tfPassword.borderStyle = UITextField.BorderStyle.none
        tfPassword.layer.addSublayer(bottomLinePassword)
        
        let bottomLineEmail = CALayer()
        bottomLineEmail.backgroundColor = UIColor.white.cgColor
        bottomLineEmail.frame = CGRect(x: 0.0, y: tfEmail.frame.height - 1, width: tfEmail.frame.width, height: 1.0)
        tfEmail.borderStyle = UITextField.BorderStyle.none
        tfEmail.layer.addSublayer(bottomLineEmail)
    }
    
    //---------------------------------------------
    // MARK: - Buttons
    //---------------------------------------------
    
    @IBAction func loginUser(_ sender: Any) {
        if !tfEmail.text!.isEmpty && !tfPassword.text!.isEmpty {
            let user = Patient()
            user.email = tfEmail.text
            user.password = tfPassword.text
            aparecerProgressBar(color: UIColor.white)
            repoAuth.loginUser(user: user, listener: self, label: "login")
        } else {
            let alert = UIAlertController(title: "Incomplete", message: "You must fill all the fields to continue with your login", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //---------------------------------------------
    // MARK: - Segue
    //---------------------------------------------
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //If needed
    }
    
    //---------------------------------------------
    // MARK: - Listeners
    //---------------------------------------------
    
    func success(label: String, objeto: Any?) {
        if label == "login" {
            desaparecerProgressBar()
            self.performSegue(withIdentifier: "loginSuccess", sender: nil)
        }
    }
    
    func fail(label: String) {
        desaparecerProgressBar()
        switch label {
        case Constants.Errors.WRONG_INFO_LOGIN:
            let alert = UIAlertController(title: "Incorrect", message: "Wrong email or password", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        case Constants.Errors.NO_INTERNET:
            let alert = UIAlertController(title: "No Internet", message: "Seems like you don´t have a stable internet connection", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        default:
            let alert = UIAlertController(title: "Error", message: "We are having issues with your request", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func update(label: String, objeto: Any?) {
        //
    }
}
