//
//  BarcodeWasRead.swift
//  Pharmapp
//
//  Created by Laura Pardo on 14/11/20.
//

import UIKit

class BarcodeWasReadViewController: BaseViewController, ServicesListener
{
    var code=0;
    var medicine: Medicine?
        let repoMedicines = MedicinesRepo()
    @IBOutlet weak var medLabel: UILabel!
    override func viewDidLoad() {
        medLabel.text="\(code)"
        getMedicinesBarCode()
        //medLabel.text=medicine?.name
    }
    func getMedicinesBarCode(){
            repoMedicines.getMedicineByBarCode(listener: self, label: Constants.Labels.GET_MEDICINE_BARCODE, barCode: code)
        }
    func setBarcode(theCode: Int)
    {
        code = theCode;
    }
    
    func success(label: String, objeto: Any?) {
        if label == Constants.Labels.GET_MEDICINE_BARCODE {
                    medicine = (objeto as! Medicine)
                    medLabel.text = medicine?.name;
                    //addButton.isHidden=false
                }
    }
    
    func fail(label: String) {
        medLabel.text = "Sorry, the medicince is not in our database"
    }
    
    func update(label: String, objeto: Any?) {
        
    }
    
    
}
