//
//  BarcodeWasRead.swift
//  Pharmapp
//
//  Created by Laura Pardo on 14/11/20.
//

import UIKit


class BarcodeWasReadViewController: BaseViewController, ServicesListener
{
    var code=0;
    var medicine: Medicine?
    let repoMedicines = MedicinesRepo()
    @IBOutlet weak var medLabel: UILabel!
    override func viewDidLoad() {
        medLabel.text="\(code)"
        getMedicinesBarCode()
        medLabel.text=medicine?.name
    }
    func getMedicinesBarCode(){
        repoMedicines.getMedicineByBarCode(listener: self, label: Constants.Labels.GET_MEDICINE_BARCODE, barCode: "\(code)")
        }

    
    func success(label: String, objeto: Any?) {
        if label == Constants.Labels.GET_MEDICINE_BARCODE {
            
            let arrayMeds = objeto as! Array<Medicine>
            if(arrayMeds.count>0){
                medicine = arrayMeds[0]
                medLabel.text = medicine?.name;
            }
            else
            {
                medLabel.text = "Medicine not found"
            }
            
            //addButton.isHidden=false
        }
    }
    
    func fail(label: String) {
        medLabel.text = "Sorry, the medicince is not in our database"
    }
    
    func update(label: String, objeto: Any?) {
        
    }
    
    
}

