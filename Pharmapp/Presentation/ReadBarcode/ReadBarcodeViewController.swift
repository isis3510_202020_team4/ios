//
//  ReadBarcodeViewController.swift
//  Pharmapp
//
//  Created by Laura Pardo on 21/10/20.
//

import AVFoundation
import UIKit

class ReadBarcodeViewController: BaseViewController, AVCaptureMetadataOutputObjectsDelegate, ServicesListener {
   // @IBOutlet weak var codeLbl: UILabel!
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!

    var code=0;
    var medicine: Medicine?
    let repoMedicines = MedicinesRepo()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.ean8, .ean13, .pdf417]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)

        captureSession.startRunning()
    }

    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(theCode: stringValue)
        }

       // dismiss(animated: true)
    }

    func found(theCode: String) {
        print("Code: "+theCode)
        
        code = Int(theCode) ?? 0;
        getMedicinesBarCode()
        
        //codeLbl.text="Code: "+code
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if( segue.identifier=="BarcodeReadSegue") {
            let viewController:MedicinesDetailViewController = segue.destination as! MedicinesDetailViewController
            viewController.actualMedicine = medicine!
        }
    }


    override var prefersStatusBarHidden: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func getMedicinesBarCode(){
        repoMedicines.getMedicineByBarCode(listener: self, label: Constants.Labels.GET_MEDICINE_BARCODE, barCode: "\(code)")
        }

    
    func success(label: String, objeto: Any?) {
        if label == Constants.Labels.GET_MEDICINE_BARCODE {
            
            let arrayMeds = objeto as! Array<Medicine>
            if(arrayMeds.count>0){
                medicine = arrayMeds[0]
                performSegue(withIdentifier: "BarcodeReadSegue", sender: self)
            }
            else
            {
            }
            
            //addButton.isHidden=false
        }
    }
    
    func fail(label: String) {
       
    }
    
    func update(label: String, objeto: Any?) {
        
    }
}
