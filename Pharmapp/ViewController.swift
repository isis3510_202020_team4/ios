//
//  ViewController.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 24/09/20.
//

import UIKit
import MapKit
import CoreLocation
import Network
import FirebaseAuth
class ViewController: BaseViewController, CLLocationManagerDelegate, ServicesListener {
    
    @IBOutlet weak var allButton: UIButton!
    
    @IBOutlet weak var tag: UILabel!
    
    @IBOutlet weak var addDrug: UIButton!
    @IBOutlet weak var pillsButton: UIButton!
    @IBOutlet weak var vitaminButton: UIButton!
    @IBOutlet weak var medRectangle: UIImageView!
    @IBOutlet weak var name: UILabel!
    let monitor = NWPathMonitor()
    let locationManager = CLLocationManager()
    var shouldAlert=true;
    var readings=0;
    let repoUser = UserRepo()
    let defaults = UserDefaults.standard
    
    var  path : UIBezierPath!
    override func viewDidLoad() {
        super.viewDidLoad()
        name.text="Amanda";
        styleButtons();
        addActionToButtons();
        styleMedRect();
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()

        DispatchQueue.global(qos: .background).asyncAfter(deadline: DispatchTime.now()+30) {
            if CLLocationManager.locationServicesEnabled() {
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                self.locationManager.startUpdatingLocation()
            }
        }
        
        self.monitor.pathUpdateHandler = { pathUpdateHandler in
            if pathUpdateHandler.status == .satisfied {
                
            } else {
                let alert = UIAlertController(title: "You're not connected to the internet", message: "Some services like barcode reading may not work", preferredStyle: .alert)

                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))

                self.present(alert, animated: true)
            }
        }

        let queue = DispatchQueue.global(qos: .background);
        monitor.start(queue: queue)
       if let user = Auth.auth().currentUser {
                   repoUser.getUser(uid: user.uid, listener: self, label: "infoUser")
               }
    }
    
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
            fatalError()
        }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

       // DispatchQueue.global(qos: .background).asyncAfter(deadline: DispatchTime.now()+60)
        //{
        let locValue: CLLocationCoordinate2D = manager.location!.coordinate
        
            self.changePlace(locValue)
        //}
    }
    
    func changePlace(_ locValue : CLLocationCoordinate2D ){

        let lat = locValue.latitude
        let roundedLat = Double(round(1000*lat)/1000)
        let lon = locValue.longitude
        let roundedLon = Double(round(1000*lon)/1000)
        if defaults.bool(forKey: "First Launch") == false
        {
        
            let defaultLat = defaults.double(forKey: "latitude")
            let defaultLon = defaults.double(forKey: "longitude")
        
        if((defaultLat != roundedLat || defaultLon != roundedLon) && shouldAlert && readings>10)
        {
            let alert = UIAlertController(title: "Did you bring your medicines?", message: "We noticed you're changing your location and wanted to remind you to bring your medicines.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alert, animated: true)
            shouldAlert=false;
        }
        readings+=1
        }
        else
        {
            defaults.set(true, forKey: "First Launch");
        }
        
        defaults.set(roundedLat, forKey: "latitude")
        defaults.set(roundedLon, forKey: "longitude")
    }
    
    func styleButtons()
    {
        allButton.layer.cornerRadius=25
        allButton.layer.borderWidth=0
        allButton.backgroundColor=#colorLiteral(red: 0.4352941176, green: 0.9137254902, blue: 0.7803921569, alpha: 1)
        allButton.frame.size=CGSize(width: 100, height: 50)
        allButton.setTitleColor(UIColor.white, for: .normal)
        allButton.isSelected=false;
        
        vitaminButton.layer.cornerRadius=25
        vitaminButton.layer.borderWidth=0
        vitaminButton.backgroundColor=UIColor.white
        vitaminButton.frame.size=CGSize(width: 100, height: 50)
        vitaminButton.setTitleColor(UIColor.black, for: .normal)
        vitaminButton.isSelected=false;
        
        pillsButton.layer.cornerRadius=25
        pillsButton.layer.borderWidth=0
        pillsButton.backgroundColor=UIColor.white
        pillsButton.frame.size=CGSize(width: 100, height: 50)
        pillsButton.setTitleColor(UIColor.black, for: .normal)
        pillsButton.isSelected=false;
    }
    func addActionToButtons() {
        allButton.addTarget(self, action: #selector(filterAll), for: .touchUpInside)
        vitaminButton.addTarget(self, action: #selector(filterVitamins), for: .touchUpInside)
        pillsButton.addTarget(self, action: #selector(filterPills), for: .touchUpInside)
    }
    @IBAction func filterAll(_ sender: UIButton) {
        allButton.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.9137254902, blue: 0.7803921569, alpha: 1)
        allButton.setTitleColor(UIColor.white, for: .normal)
        vitaminButton.backgroundColor = UIColor.white
        vitaminButton.setTitleColor(UIColor.black, for: .normal)
        pillsButton.backgroundColor = UIColor.white
        pillsButton.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func filterVitamins(_ sender: UIButton) {
        vitaminButton.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.9137254902, blue: 0.7803921569, alpha: 1)
        vitaminButton.setTitleColor(UIColor.white, for: .normal)
        allButton.backgroundColor = UIColor.white
        allButton.setTitleColor(UIColor.black, for: .normal)
        pillsButton.backgroundColor = UIColor.white
        pillsButton.setTitleColor(UIColor.black, for: .normal)
    }
    @IBAction func filterPills(_ sender: UIButton) {
        pillsButton.backgroundColor = #colorLiteral(red: 0.4352941176, green: 0.9137254902, blue: 0.7803921569, alpha: 1)
        pillsButton.setTitleColor(UIColor.white, for: .normal)
        allButton.backgroundColor = UIColor.white
        allButton.setTitleColor(UIColor.black, for: .normal)
        vitaminButton.backgroundColor = UIColor.white
        vitaminButton.setTitleColor(UIColor.black, for: .normal)
    }

    @IBAction func logOut(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            performSegue(withIdentifier: "logoutSession", sender: nil)
        }
        catch let e{
            print("e: \(e.localizedDescription)")
        }
    }


    func styleMedRect()
    {
        medRectangle.layer.cornerRadius=60
        medRectangle.clipsToBounds=true
        medRectangle.tintColor=UIColor.white
    }
    

    func success(label: String, objeto: Any?) {
        switch label {
        case "infoUser":
            let patient = objeto as! Patient
            name.text = patient.name
        default:
            let alert = UIAlertController(title: "Error", message: "We are having issues with your request", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Try again", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func fail(label: String) {
        //
    }

    func update(label: String, objeto: Any?) {
        //
    }
}
