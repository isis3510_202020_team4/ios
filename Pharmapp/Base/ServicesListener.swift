//
//  BaseViewController.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 24/10/20.
//

import Foundation

protocol ServicesListener: class {
    func success(label: String, objeto: Any?)
    func fail(label: String)
    func update(label: String, objeto: Any?)
}
