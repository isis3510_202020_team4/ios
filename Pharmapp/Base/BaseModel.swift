//
//  BaseModel.swift
//  musapp
//
//  Created by EdgardEduardo on 6/24/19.
//  Copyright © 2019 Musa. All rights reserved.
//

import Foundation

class BaseModel: NSObject{
    var uidDocumento: String?
    var uidColeccion: String?
    
    /// Este método se DEBE SOBRESCRIBIR para crear una instancia de una *instancia* de `BaseModel` a partir de un *diccionario*(`Dict`) cuyas *llaves* son los atributos de la clase a instanciar y cuyos *valores* son los *valores* de esos *atributos*.
    ///
    /// Se usa para convertir la respuesta que hace firebase a veces, a una *clase* de la carpeta *Modelos*.
    ///
    /// - Parameter mapa: un *diccionario*(`Dict`) que contiene los atributos de la clase que se quiere instanciar
    required convenience init(mapa: [String: Any]){
        preconditionFailure("This constructor must be overwritten")
    }
}
