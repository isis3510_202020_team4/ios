//
//  BaseProxy.swift
//  musapp
//
//  Created by EdgardEduardo on 6/24/19.
//  Copyright © 2019 Musa. All rights reserved.
//

import Foundation
import SystemConfiguration
import FirebaseAuth

class BaseProxy{
    //---------------------------------------------
    // MARK: - Métodos
    //---------------------------------------------
    
     /// Comprueba la conexión a internet
    /// - remark: código obtenido de [ésta página](https://www.webnexs.com/blog/kb/check-internet-connection-using-swift/)
     /// - Returns: `false` si **no hay** conexión a internet
     final func conexionInternet() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
}
