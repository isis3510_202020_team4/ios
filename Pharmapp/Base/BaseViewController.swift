//
//  BaseViewController.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 8/11/20.
//

import Foundation
import UIKit
class BaseViewController: UIViewController {
    
    /// Hace **aparecer** el circulo que indica el progreso de una tarea
    /// La vista que se utiliza es un `UIActivityIndicatorView`
    
    private var progressIndicator : UIActivityIndicatorView? = nil
    
    final func aparecerProgressBar(color: UIColor = Constants.Colors.titlesGreen){
        if progressIndicator == nil{
            progressIndicator = UIActivityIndicatorView(style: .whiteLarge)
            progressIndicator!.center = self.view.center
            progressIndicator!.hidesWhenStopped = true
            self.view.addSubview(progressIndicator!)
        }
        progressIndicator!.color = color
        progressIndicator!.startAnimating()
        self.view.isUserInteractionEnabled = false
        let delay = DispatchTime.now() + .seconds(30)
        DispatchQueue.main.asyncAfter(deadline: delay) {
            self.desaparecerProgressBar()
        }
    }
    
    /// Hace **desaparecer** el circulo que indica el progreso de una tarea
    /// La vista que se utiliza es un `UIActivityIndicatorView`
    final func desaparecerProgressBar(){
        navigationController?.navigationBar.isUserInteractionEnabled = true
        self.view.isUserInteractionEnabled = true
        progressIndicator?.stopAnimating()
    }
}
