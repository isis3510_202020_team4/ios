//
//  Constantes.swift
//  Pharmapp
//
//  Created by Sebastián Millán on 25/09/20.
//

import Foundation
import UIKit

final class Constants{
    //Colors
    public class Colors {
        static let colorAzul:UIColor = UIColor(red: 52/255, green: 97/255, blue: 207/255, alpha: 1)
        static let buttonsGreen:UIColor = UIColor(red: 117/255, green: 234/255, blue: 195/255, alpha: 1)
        static let titlesGreen:UIColor = UIColor(red: 6/255, green: 183/255, blue: 170/255, alpha: 1)
        static let gradientStart:UIColor = UIColor(red: 117/255, green: 234/255, blue: 195/255, alpha: 1)
        static let gradientEnd:UIColor = UIColor(red: 6/255, green: 183/255, blue: 170/255, alpha: 1)
        static let greyText:UIColor = UIColor(red: 169/255, green: 169/255, blue: 169/255, alpha: 1)
    }
    
    public class Errors {
        static let NO_INTERNET = "noInternet";
        static let LISTENER_ERROR = "listenerError";
        static let GOOGLE_AUTH_ERROR = "googleAuthError"
        static let WRONG_INFO_LOGIN = "wrongInfoLogin"
        static let WRONG_INFO_REGISTER = "wrongInfoRegister"
        static let CLOUD_ERROR = "cloudError"
        static let NOT_FOUND = "notFound"
    }
    
    public class Labels {
        static let GET_MEDICINES = "getMedicines";
        static let GET_MEDICINE_BARCODE = "getMedicineByBarCode";
    }
    
    public class CloudFunctions {
        static let RISK = "Risk";
        static let RISK_DESCRIPTION = "We found a possible adverse effect by using this medicine with another one that you are consuming already, are you sure you want to added anyway?";
        static let ADDED = "Added";
        static let ADDED_DESCRIPTION = "Your medicine was added successfully, now you can see it in your calendar";
        static let NO_MEDICINE = "No medicine found";
        static let NO_MEDICINE_DESCRIPTION = "We could not find any medicine as the one you are trying to add";
        static let NO_ID_MEDICINE = "No id medicine receive";
        static let NO_ID_MEDICINE_DESCRIPTION = "We could not find any medicine with the id that you are trying to add";
        static let UNAUTHORIZED = "Unauthorized";
        static let UNAUTHORIZED_DESCRIPTION = "You do not have access to this functionality, please authenticate before making the request";
    }
}
